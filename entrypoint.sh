#!/bin/bash

# Run the Flask app with Gunicorn
gunicorn --config gunicorn_config.py app:app
