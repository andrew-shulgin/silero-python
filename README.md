# Silero VAD server

A [Silero VAD](https://github.com/snakers4/silero-vad) microservice which provides speech probability for uploaded audio filess.

Written as a [Flask](https://flask.palletsprojects.com/en/2.3.x) app and run using [Gunicorn](https://gunicorn.org/).


## How to run

### As a Docker container
Make sure Docker is installed: https://docs.docker.com/engine/install/

Then run the following commands:
```
git clone https://gitlab.com/tayabjamil423/silero-python.git silero-vad-server
cd silero-vad-server
docker-compose build
docker-compose up -d
```

#### Viewing container logs in real time
```
docker-compose logs -f
```

#### Stopping container
```
docker-compose down
```

#### Configuring workers
The number of `Gunicorn` workers to employ can be specified through the `WORKERS` environment variable in the [.env file](.env)
```
WORKERS=2
```

## Configuring

### Gunicorn
Gunicorn can be configured through the [gunicorn_config.py](/gunicorn_config.py) file.

Additional Gunicorn settings can be referenced [here](https://docs.gunicorn.org/en/stable/settings.html).

## How to test

Make a request to:
```
Method: POST
Endpoint: http://localhost:5001/speech_probability
Body: form-data
Request files:
    audio_data - audio/wav
```

cURL:
```
curl -X POST -F "audio_file=@.test/audio.wav" http://127.0.0.1:5001/speech_probability
```

```
cd test
sh test.sh
```

Success response example:
```
{"average_speech_probability":0.8855901585109678,"success":true}
```
