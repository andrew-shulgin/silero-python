# Use the official Python 3.11.5 image
FROM python:3.11.5


# Set the working directory inside the container
WORKDIR /app

# Copy the entire application directory into the container
COPY . .


# Install SoX
RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update && \
    apt-get install -y sox ffmpeg && \
    rm -rf /var/lib/apt/lists/*

# Install torchaudio
RUN pip install torchaudio --extra-index-url https://download.pytorch.org/whl/cpu

# Set the torchaudio backend to 'sox_io'
RUN python -c "import torchaudio; torchaudio.set_audio_backend('sox_io');"


# Install deps specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Clone the silero-vad repo to the app directory
RUN git clone https://github.com/snakers4/silero-vad.git silero-vad


# Expose port 5001
EXPOSE 5001

ENV PYTHONUNBUFFERED=1

# Run the app using Gunicorn
CMD ["./entrypoint.sh"]
