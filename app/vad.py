from time import time
from dataclasses import dataclass

import torch
import torchaudio

from . import utils as myutils

logger = myutils.create_logger("vad")

print("torchaudio backends:", str(torchaudio.list_audio_backends()));

@dataclass
class InferSpeechProbResult:
    speech_prob: float
    infer_time: int


torch.set_num_threads(1)

USE_ONNX = True  # change this to True if you want to test onnx model
model, utils = torch.hub.load(
    repo_or_dir="silero-vad",
    source="local",
    trust_repo=True,
    model="silero_vad",
    force_reload=False,
    onnx=USE_ONNX,
    force_onnx_cpu=True,
)

(get_speech_timestamps, _, read_audio, *_) = utils

sampling_rate = 8000  # [8000, 16000]


def infer_speech_prob(file_path, tag):
    logger_name = f"infer_speech_prob:{tag}"
    child_logger = logger.getChild(logger_name)

    child_logger.info("Inferring speech probability...")

    sampling_rate = 8000
    myutils.check_file(file_path)
    wav = read_audio(file_path, sampling_rate=sampling_rate)

    start_time = time()

    speech_probs = []
    window_size_samples = 256  # use 256 for 8000 Hz model
    for i in range(0, len(wav), window_size_samples):
        chunk = wav[i : i + window_size_samples]
        if len(chunk) < window_size_samples:
            break
        speech_prob = model(chunk, sampling_rate).item()
        speech_probs.append(speech_prob)
    model.reset_states()  # reset model states after each audio

    end_time = time()
    infer_time = round((end_time - start_time) * 1000)

    avg_speech_prob = arr_average(speech_probs) or 0

    child_logger.info(
        f"Infer success - Speech probability: {avg_speech_prob:.3f}, Time Taken: {infer_time}ms"
    )

    result = InferSpeechProbResult(speech_prob=avg_speech_prob, infer_time=infer_time)

    return result


def arr_average(arr):
    if not arr:
        return 0

    array_sum = sum(arr)
    average = array_sum / len(arr)
    return average
