import logging
import tempfile
import os


def create_logger(name):
    logger = logging.getLogger(name)
    handler = logging.StreamHandler()
    handler.setFormatter(
        logging.Formatter("%(levelname)s: %(asctime)s: %(name)s  %(message)s")
    )
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    return logger


def load_audio_tempfile(data):
    fp = tempfile.NamedTemporaryFile()
    fp.write(data)
    fp.flush()

    return fp


def check_file(file_path):
    if os.path.exists(file_path):
        try:
            file_size = os.path.getsize(file_path)
            print(f"File '{file_path}' exists and its size is {file_size} bytes.")
        except Exception as e:
            print(f"An error occurred while getting file size: {str(e)}")
    else:
        print(f"File '{file_path}' does not exist.")
